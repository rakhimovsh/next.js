import React, {useEffect, useState, KeyboardEvent} from 'react';
import classNames from "classnames";
import {RatingProps} from "./Rating.props";
import StartIcon from "./star.svg";
import styles from "./Rating.module.css";

const Rating = ({rating, setRating, isEditable, ...props}: RatingProps): JSX.Element => {
    const [ratingArray, setRatingArray] = useState<JSX.Element[]>(new Array(5).fill(<></>));

    useEffect(()=>{
        constructorRating(rating);
    }, [rating]);
    const constructorRating = (currentRating: number): void =>{
        const updatedArray = ratingArray.map((r, i)=>{
           return <span onMouseEnter={()=> changeDisplay(i+1)}
                        onMouseLeave={()=> changeDisplay(rating)}
                        onClick={()=> onClick(i+1)}
                        className={classNames(styles.star, {
                            [styles.fill]: currentRating > i,
                            [styles.editable]: isEditable
                        })}
           >
               <StartIcon
                          tabIndex={isEditable ? 0:-1}
                          onKeyDown={(evt: KeyboardEvent<SVGElement>)=> keydownHandle(evt, i+1)}
               />
           </span>;

        });
        setRatingArray(updatedArray);
    };
    const changeDisplay = (i: number)=>{
        if(!isEditable) return;
        constructorRating(i);
    };
    const onClick = (i: number)=>{
      if(!isEditable || !setRating) return;
      setRating(i);
    };

    const keydownHandle = (evt: KeyboardEvent<SVGElement>, i: number)=>{
        if(!isEditable || !setRating) return;
        if(evt.code === "Space"){
            setRating(i);
        }
    };

    return (
        <div {...props}>
            {
                ratingArray.map((el, i)=>{
                    return <span key={i}>{el}</span>;
                })
            }
        </div>
    );
};

export default Rating;