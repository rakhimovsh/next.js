import classNames from "classnames";

import {PTagProps} from "@/components/PTag/PTag.props";
import styles from "./PTag.module.css";


export default ({children,size = "M", className, ...props}:PTagProps):JSX.Element =>{
    return <p className={classNames(styles.p, className, {
        [styles.S]: size === "S",
        [styles.M]: size === "M",
        [styles.L]: size === "L"
    })} {...props}>{children}</p>;
};