import classNames from "classnames";

import {ButtonProps} from "@/components/Button/Button.props";
import styles from "./Button.module.css";


export default ({children, appearance, className, arrow = "none", ...props}: ButtonProps):JSX.Element=>{
    return (
        <button
            className={classNames(styles.button, className,
                {
                    [styles.primary]: appearance === "primary",
                    [styles.ghost]: appearance === "ghost"
                }
            )}
            {...props}
        >
            {children}
            {arrow != "none" && <span className={classNames(styles.arrow, {
                [styles.down]: arrow === "down"
            })}>
                <img src="/arrow.svg" alt="arrow icon"/>
            </span>}
        </button>
    );
};