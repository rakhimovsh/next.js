import classNames from "classnames";

import styles from "./Tag.module.css";
import {TagProps} from "./Tag.props";


export default ({children,size = "S",color, className,href, ...props}:TagProps):JSX.Element =>{
    return <div className={classNames(styles.tag, className,{
        [styles.M]: size === "M",
        [styles.S]: size === "S",
        [styles.green]: color==="green",
        [styles.gray]: color === "gray",
        [styles.red]: color === "red",
        [styles.primary]: color === "primary",
        [styles.ghost]: color === "ghost"
    })} {...props}>
        {
            href ?
                <a href={href}>{children}</a>
                :
                <>{children}</>
        }
    </div>;
};