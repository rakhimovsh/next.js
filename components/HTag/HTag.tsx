import {HTagProps} from "@/components/HTag/HTag.props";
import styles from "./HTag.module.css";
import classNames from "classnames";

export default ({tag, children, className}: HTagProps): JSX.Element =>{
     switch (tag){
          case "h1":
               return <h1 className={classNames(styles.h1, className)}>{children}</h1>;
          case "h2":
               return <h2 className={classNames(styles.h2, className)}>{children}</h2>;
          case "h3":
               return <h3 className={classNames(styles.h3, className)}>{children}</h3>;
          default:
               return <>{children}</>;
     }
};