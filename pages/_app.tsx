import {AppProps} from "next/app";
import Head from "next/head";
import "@/styles/globals.css";

export default function ({Component, pageProps}: AppProps):JSX.Element{
    return (
        <>
            <Head>
                <title>MyTop</title>
            </Head>
            <Component {...pageProps} />
        </>
    );
}