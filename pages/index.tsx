import Head from "next/head";
import Htag from "@/components/HTag/HTag";
import Button from "@/components/Button/Button";
import PTag from "@/components/PTag/PTag";
import Tag from "@/components/Tag/Tag";
import Rating from "@/components/Rating/Rating";
import {useState} from "react";
import Layout from "@/layout/Layout";
import {GetStaticProps} from "next";
import axios from "axios";
import {MenuItem} from "@/interfaces/menu.interface";

function Home ({menu}: HomeProps): JSX.Element{
    const [rating, setRating] = useState<number>(3);
    return (
        <>
            <Head>
                <title>Hello from next.js</title>
            </Head>
            <Htag tag="h1">cbd</Htag>
            <Button appearance="primary">Button</Button>
            <Button appearance="ghost" arrow="right">Button</Button>
            <PTag size="L">lsslnhnhwntrrrrrrrrrrrrrrrbrehbeurillllllllllllllllllllllllllllllllllllllbhrt</PTag>
            <Tag color="primary">Primary</Tag>
            <Tag color="gray">gray</Tag>
            <Tag color="green">green</Tag>
            <Tag color="red" size="M">red</Tag>
            <Rating rating={rating} isEditable setRating={setRating}/>

        </>
    );
}

export default Layout(Home);

export const getStaticProps: GetStaticProps = async ()=>{
    const firstCategory = 0;
    const api = process.env.NEXT_PUBLIC_DOMAIN;

    const {data:menu} = await axios.post<MenuItem[]>(api + "/api/top-page/find", {firstCategory});
    return {
        props: {
            menu, firstCategory
        }
    };
};

interface HomeProps extends Record<string, unknown>{
    menu: MenuItem[],
    firstCategory: number
}