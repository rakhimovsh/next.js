import Layout from "@/layout/Layout";
import {GetStaticPaths, GetStaticProps, GetStaticPropsContext} from "next";
import axios from "axios";
import {MenuItem} from "@/interfaces/menu.interface";
import {PageModel} from "@/interfaces/page.interface";
import {NextParsedUrlQuery} from "next/dist/server/request-meta";
import {ProductModel} from "@/interfaces/product.interface";

const firstCategory = 0;
const api = process.env.NEXT_PUBLIC_DOMAIN;



function Course ({menu, products}: CourseProps): JSX.Element{
    return (
        <>
            <ul>
                {
                    menu.map(el=> (<li key={el._id.secondCategory}>{el._id.secondCategory}</li>))
                }
            </ul>
            {
                products && products.length
            }
        </>
    );
}

export default Layout(Course);


export const getStaticPaths: GetStaticPaths = async ()=>{
    
    const {data:menu} = await axios.post<MenuItem[]>(api + "/api/top-page/find", {firstCategory});
    const paths = menu.flatMap(m=> m.pages.map(p=> "/courses/" + p.alias))
    return {
      paths,
      fallback: true
    };
};
export const getStaticProps: GetStaticProps = async ({params}: GetStaticPropsContext<NextParsedUrlQuery>)=>{

    if(!params) {
        return {
            notFound: true
        };
    }
    const {data:menu} = await axios.post<MenuItem[]>(api + "/api/top-page/find", {firstCategory});
    const {data:page} = await axios.get<PageModel>(api + "/api/top-page/byAlias/" + params.alias);
    const {data:products} = await axios.post<ProductModel[]>(api + "/api/product/find", {category: page.category, limit: 10});
    return {
        props: {
            menu,
            page,
            products
        }
    };
};

interface CourseProps extends Record<string, unknown>{
    menu: MenuItem[],
    page: PageModel,
    products: ProductModel[]
}