import {FooterProps} from "./Footer.props";
import classNames from "classnames";
import styles from "./Footer.module.css";
import {format} from "date-fns";


export default ({className,...props}: FooterProps): JSX.Element =>{
    return <footer className={classNames(className , styles.footer)} {...props}>
            <p className={classNames(styles.p)}>OwlTop © 2020 - {format(new Date(), "yyyy")} Все права защищены</p>
            <p className={classNames(styles.p)}>Пользовательское соглашение</p>
            <p className={classNames(styles.p)}>Политика конфиденциальности</p>
        </footer>;
};