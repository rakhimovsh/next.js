import {NavbarProps} from "./Navbar.props";
import Menu from "@/layout/Menu/Menu";


export default ({...props}: NavbarProps): JSX.Element =>{
    return <div {...props}>
            <Menu/>
        </div>;
};