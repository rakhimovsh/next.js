import {LayoutProps} from "./Layout.props";
import Header from "@/layout/Header/Header";
import Navbar from "@/layout/Navbar/Navbar";
import Footer from "@/layout/Footer/Footer";
import {FunctionComponent} from "react";
import styles from './Layout.module.css';
import {AppContextProvider, IAppContext} from "@/context/app.context";


const Layout = ({children}: LayoutProps): JSX.Element =>{
    return <div className={styles.wrapper}>
            <Header className={styles.header}/>
                <Navbar className={styles.navbar}/>
                <main className={styles.main}>
                    {children}
                </main>
            <Footer className={styles.footer}/>
        </div>;
};

export default <T extends Record<string, unknown> & IAppContext>(Component: FunctionComponent<T>) =>{
    return function (props: T):JSX.Element {
        return(
            <AppContextProvider menu={props.menu} firstCategory={props.firstCategory}>
                <Layout>
                    <Component {...props}/>
                </Layout>
            </AppContextProvider>
        );
    };
};