import styles from './Menu.module.css';
import {useContext} from "react";
import {AppContext} from "@/context/app.context";
import {FirstLevelMenu, PageItem} from "@/interfaces/menu.interface";
import CoursesIcon from "./icons/courses.svg";
import ServicesIcon from "./icons/services.svg";
import BooksIcons from "./icons/books.svg";
import ProductsIcon from './icons/products.svg';
import {TopLevelCategory} from "@/interfaces/page.interface";
import classNames from "classnames";

const firstLevelMenu: FirstLevelMenu[] = [
    {route: "courses", name: "Курсы", icon: <CoursesIcon/>, id: TopLevelCategory.Courses},
    {route: "services", name: "Сервисы", icon: <ServicesIcon/>, id: TopLevelCategory.Services},
    {route: "books", name: "Книги", icon: <BooksIcons/>, id: TopLevelCategory.Books},
    {route: "products", name: "Продукты", icon: <ProductsIcon/>, id: TopLevelCategory.Products}
];

export default (): JSX.Element =>{
    const {menu, setMenu, firstCategory} = useContext(AppContext);

    const buildFirstLevel = () => {
        return (
            <>
                {firstLevelMenu.map(m => (
                    <div key={m.route}>
                        <a href={`/${m.route}`}>
                            <div className={classNames(styles.firstLevel, {
                                [styles.firstLevelActive]: m.id == firstCategory
                            })}>
                                {m.icon}
                                <span >{m.name}</span>
                            </div>
                        </a>
                        {m.id == firstCategory && buildSecondLevel(m.route)}
                    </div>
                ))}
            </>
        );
    };

    const buildSecondLevel = (menuRoute: string) => {
        return (
            <div className={styles.secondBlock}>
                {menu.map(m => (
                    <div key={m._id.secondCategory}>
                        <div className={styles.secondLevel}>{m._id.secondCategory}</div>
                        <div className={classNames(styles.secondLevelBlock, {
                            [styles.secondLevelBlockOpened]: m.isOpened
                        })}>
                            {buildThirdLevel(m.pages, menuRoute)}
                        </div>
                    </div>
                ))}
            </div>
        );
    };

    const buildThirdLevel = (pages: PageItem[], route: string) => {
        return (
            pages.map(p => (
                <a key={p._id} href={`/${route}/${p.alias}`} className={classNames(styles.thirdLevel, {
                    [styles.thirdLevelActive]: false
                })}>
                    {p.category}
                </a>
            ))
        );
    };


    return <div className={styles.menu}>
            {buildFirstLevel()}
        </div>;
};